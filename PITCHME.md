---?image=assets/img/blazor.png&size=40%

### Blazor

---

### Blazor

@snap[west span-45]
@box[bg-purple text-white](Full-stack web developpemnt with .NET)
@snapend

@snap[east span-45]
@box[bg-orange text-white](No plugin or code transpilation)
@snapend

@snap[south-west span-45]
@box[bg-pink text-white](Works with all modern browers including mobile browsers)
@snapend

@snap[south-east span-45]
@box[bg-blue text-white](Browser + Razor &rightarrow; Blazor)
@snapend

+++

### https://blazor.net

<iframe class="stretch" data-src="https://blazor.net"></iframe>

+++

@snap[midpoint]
### Demo : Blazor
@snapend

+++?image=assets/img/blazor-server.png&size=auto
### Server side rendering

---?image=assets/img/WebAssembly.png&size=auto
#### @color[#e49436](WASM), your browser is your OS @color[red](@fa[heart])

+++?image=assets/img/WASM.png&size=auto

+++

### https://webassembly.org

<iframe class="stretch" data-src="https://webassembly.org/"></iframe>

+++?image=assets/img/blazor-webassembly.png&size=auto
### Client side rendering

+++

@snap[midpoint]
### Demo : Blazor WebAssembly
@snapend

---

### Blazor Key Points

@snap[west span-45]
@box[bg-purple text-white](.NET Standard libraries and JS/.NET interop)
@snapend

@snap[east span-45]
@box[bg-purple text-white](Reusable Components)
@snapend

@snap[south-west span-45]
@box[bg-purple text-white](MVVM Pattern)
@snapend

@snap[south-east span-45]
@box[bg-purple text-white](Debugging)
@snapend

+++?image=assets/img/blazor-roadmap.webp&size=auto

### Blazor's roadmap

+++

### Active community

@snap[midpoint]
https://github.com/AdrienTorris/awesome-blazor
@snapend

---?color=#0F2862

@snap[midpoint]
## Thanks @color[orange](@fa[grin-beam])
@snapend